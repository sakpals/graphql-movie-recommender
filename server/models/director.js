const mongoose = require("mongoose");

const directorSchema = new mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
	name: { type: String, required: true },
	type: { type: String },
	movies: { type: [String] }
});

const Director = mongoose.model("Director", directorSchema, "directors");

module.exports = Director;
