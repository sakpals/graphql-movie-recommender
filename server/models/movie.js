const mongoose = require("mongoose");

const movieSchema = new mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
	name: { type: String, required: true },
	type: { type: String },
	rating: { type: Number },
	genres: { type: [String] },
	directors: { type: [String] }
});

const Movie = mongoose.model("Movie", movieSchema, "movies");

module.exports = Movie;
