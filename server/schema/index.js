const graphql = require("graphql");
const {
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
  GraphQLFloat,
  GraphQLID,
  GraphQLList,
} = graphql;
const {
  getDirectors,
  getDirectorById,
} = require("../controllers/director.controller");
const { getMovies, getMovieById } = require("../controllers/movie.controller");

const MovieType = new GraphQLObjectType({
  name: "Movie",
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    rating: { type: GraphQLFloat },
    genres: { type: new GraphQLList(GraphQLString) },
    directors: {
      type: new GraphQLList(DirectorType),
      async resolve(parent, args) {
        const directors = await getDirectors();
        return directors.filter((d) =>
          parent.directors.some((id) => id === d.id)
        );
      },
    },
  }),
});

const DirectorType = new GraphQLObjectType({
  name: "Director",
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    movies: {
      type: new GraphQLList(MovieType),
      async resolve(parent, args) {
        const movies = await getMovies();
        return movies.filter((m) => parent.movies.some((id) => id === m.id));
      },
    },
  }),
});

const RootQuery = new GraphQLObjectType({
  name: "RootQuery",
  fields: {
    director: {
      type: DirectorType,
      args: { id: { type: GraphQLID } },
      async resolve(parent, args) {
        return await getDirectorById(args.id);
      },
    },
    movie: {
      type: MovieType,
      args: { id: { type: GraphQLID } },
      async resolve(parent, args) {
        // THIS IS WHERE IT RETRIEVES DATA FROM DB
        return await getMovieById(args.id);
      },
    },
    directors: {
      type: new GraphQLList(DirectorType),
      resolve(parents, args) {
        return getDirectors();
      },
    },
    movies: {
      type: new GraphQLList(MovieType),
      async resolve(parents, args) {
        return await getMovies();
      },
    },
  },
});

module.exports = new GraphQLSchema({
  query: RootQuery,
});
