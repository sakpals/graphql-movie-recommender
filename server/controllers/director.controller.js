const Director = require("../models/director");

getDirectors = function () {
  try {
    return Director.find({}).lean();
  } catch (e) {
    console.log("error getting directors - ", e);
  }
};

getDirectorById = function (id) {
  try {
    return Director.findOne({ id }).lean();
  } catch (e) {
    console.log(`error getting director by id: ${id} - `, e);
  }
};

module.exports = { getDirectors, getDirectorById };
