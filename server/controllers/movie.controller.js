const Movie = require("../models/movie");

getMovies = function () {
  try {
    return Movie.find({}).lean();
  } catch (e) {
    console.log("error getting movies - ", e);
  }
};

getMovieById = function (id) {
  try {
    return Movie.findOne({ id }).lean();
  } catch (e) {
    console.log(`error getting movie by id: ${id} - `, e);
  }
};

module.exports = { getMovies, getMovieById };
